# diagram.py
from diagrams import Diagram, Cluster
from diagrams.onprem.iac import Ansible
from diagrams.onprem.vcs import Gitlab
from diagrams.gcp.compute import KubernetesEngine as GKE
from diagrams.gcp.compute import ComputeEngine as GCE
from diagrams.gcp.network import LoadBalancing
from diagrams.programming.framework import Flask
from diagrams.outscale.network import InternetService
from diagrams.onprem.database import MongoDB




with Diagram("Titanic API", show=False, direction="LR"):

    with Cluster("Local"):
        ansible = Ansible("Trigger")

    # GCE
    with Cluster("Google Cloud", direction="TB"):

        gce = GCE("Google CLoud Engine")
    
        # Big GKE Group
        with Cluster("GKE", direction="TB"):

            gke = GKE("GKE Cluster")

                

            with Cluster("namespace mongo", direction="LR"):
                mongo_cluster = MongoDB("Mongo RS Pod-0")

                mongo_svc = LoadBalancing("Mongo CLusterIP")

            with Cluster("namespace api\nTitanic API endpoint", direction="LR"):
                front_ha = [Flask("Pod-0\nflask+uwsgi\nnginx:6000/tcp"),
                            Flask("Pod-1\nflask+uwsgi\nnginx:6000/tcp"),
                            Flask("Pod-N\nflask+uwsgi\nnginx:6000/tcp")]

                front_svc = LoadBalancing("API LoadBlancer")

    with Cluster("Internet"):
        internet = InternetService("Internet")



        
    ansible >> gce
    ansible >> gke

    
    
    

    
    
    
 
    
    mongo_cluster << mongo_svc
    mongo_svc << front_ha
    front_ha << front_svc
    front_svc << internet
