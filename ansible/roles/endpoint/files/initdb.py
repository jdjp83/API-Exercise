import requests
import os
import json

host = os.environ['ENDPOINT_HOST']
url = "http://{}/people".format(host)

headers = {
  'Content-Type': 'application/json'
}


response = requests.request("GET", url, headers=headers)
if len(response.json()) < 10:
    print("Less than 10 registries in DB. Populating")
    with open('roles/endpoint/files/titanic.json') as people:
        for person in people.readlines():
            response = requests.request("POST", url, headers=headers, data=person)
            print(response.text)
else:
    print("{} entries in DB. Not populating.".format(len(response.json())))
