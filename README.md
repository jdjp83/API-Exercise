# 1. API-exercise

This the documentation for my implementation of the Test-API project as part of my application process to join Container Solutions.


# 2. Table of Content
<!-- TOC -->

- [1. API-exercise](#1-api-exercise)
- [2. Table of Content](#2-table-of-content)
- [3. Who is me?](#3-who-is-me)
- [4. PREMISES](#4-premises)
- [5. Architecture](#5-architecture)
    - [5.1. MongoDB Backend](#51-mongodb-backend)
    - [5.2. Titanic API endpoint](#52-titanic-api-endpoint)
        - [5.2.1. nginx](#521-nginx)
        - [5.2.2. titanic_api](#522-titanic_api)
- [6. Ansible](#6-ansible)
    - [6.1. Good to know](#61-good-to-know)
    - [6.2. Roles](#62-roles)
        - [6.2.1. cluster](#621-cluster)
        - [6.2.2. mongo](#622-mongo)
        - [6.2.3. endpoint](#623-endpoint)
- [7. Accessing the service](#7-accessing-the-service)
- [8. Improvement points / ToDo](#8-improvement-points--todo)

<!-- /TOC -->

# 3. Who is me?
"Me" is Jesús Daniel Jiménez Paniagua, but I prefer you call me just Dani. I was born in [Plasencia](https://goo.gl/maps/1ETmH8uxF89RNCB69), Extremadura, Spain, the 28th of July of 1983. I'm 37.

I'm a technology enthusiast who have played many different roles in the IT industry since 2007:
- IT Security analyst and consultant
- Enterpreneur running my own little Wireless Internet Service Provider company.
- [Freelance](https://jdani.eu) providing DevOps/SRE and [Splunk](https://splunk.com) services.
- Splunk consultan
- SRE/DevOps
- Project Manager
- Research project manager

But there is one common factor in all my positions, and I think here are the skills that push my as a pplicant to the next level:
- Despite of my role, I'm always Linux/SySAdmin related. Even when coding.
- When technical difficulty arises in my team, usually it's me the first that tries to solve the situation.
- I automates by default. Not since DevOps is a buzz word or since cloud is the way to go. I try to automate everything since the very beginning of my professional career.
- I love having fun and I have fun with IT. If you put it together... I really like what I do.


Reading and running this project I just would like you have as much fun as I had creating it. I really will try my best :)

# 4. PREMISES
* The project is described in the original [README](docs/README_ORIG.md) file, so I will asume you have read it and you are aware of the details.
* The system running these plybooks needs this software:
    * kubectl
    * gclolud
    * ansible
        * ansible-galaxy collection install google.cloud
        * ansible-galaxy collection install community.general
    * helm
    * python modules in the same env than the python interpreter running ansible:
        * openshift-client-python


# 5. Architecture

The main architecture is splitted in 2 layers:
* Data Backend: Supported by a MongoDB Replica Set
* API Frontend: Supported by a Flask API served with nginx

Apart from that, there is another layer, the Log collection/monitoring layer, based in Fluentd + Elastic + Kibana.

The idea is to trigger everything with an Ansible playbook and get everything up and running and accesible from Internet. Find below the architecture diagram:
![Titanic API](docs/titanic_api.png "Titanic API Architecture diagram")

> **Discalimer**: Re-reading teh MD doc from GitLab I just discovered Mermaid to generate embedded graphs. I wasn't aware of that, so I used [diagrams](https://diagrams.mingrammer.com/docs/getting-started/installation). The file to generate the graph is [docs/architecture.py](docs/architecture.py).



## 5.1. MongoDB Backend
First things first. For this project I was tempted to use a file-based DB like SQLite or even an in-memory CSV written to disk when modified. Finally I decided to use MongoDB because I wanted to build a more real-world project. SQLite3 is not as flexible as MongoDB and it is more likely you, at Container Solutions, works with Mongo. So that was the election.

That said, the mongo architecture is a simple standalone that uses MongoDB Official images.


> NOTE 1: I invested a few hours trying to get up and working replicaset architecture using helm, but could not login into it after being deployed. There is a branch with this code. It is called __feature_mongo-replicaset__.

> NOTE 1: There is a script to populate the database loaded via configMap into `/docker-entrypoint-initdb.d/initdb.sh`. It has execution permissions and it is supossed it should be trigger when a container starts but, for any reason, it is not triggered. I think it is worth it to take a look to the script for one of those "tips" it was asked for in the exercise. The titanic.csv file is downloaded using the command `openssl`. I always try to avoid building custom images and, in this case, no `curl`, `wget`, `ftp`, `python`, or so were included in the image to download the file. That's why I used `openssl`. Not building a custom image just to include a script is the reason too why the initdb.sh script is mounted via configMap. The config files are there for revision.

## 5.2. Titanic API endpoint
This pod is the one connecting the data with the users serving a flask api via flask+uwsgi <---> nginx reverse proxy.

The pod can be escalated (manually or with hpa), both pods are stateless. Runs in the namespace "api".

Each pod contains two containers:

### 5.2.1. nginx
This container uses the official nginx image [nginx:1.18.0-alpine](https://github.com/nginxinc/docker-nginx/blob/3fb70ddd7094c1fdd50cc83d432643dc10ab6243/stable/alpine/Dockerfile). It serves a reverse proxy serving the data provided by the container titanic_api through the port 6000/tcp. It is an internal pod communication, so it is done via localhost and secure by default.

The nginx provides this service publicly at https://titanic.jdani.eu/api.

nginx was placed at a different containers to reduce coupling.

The config file is loaded via configmap into __/etc/nginx/conf.d/default__

### 5.2.2. titanic_api
This containers runs the main piece of code of the service. The flask API. It is __based__ in [this project](https://github.com/paurakhsharma/flask-rest-api-blog-series/tree/master/Part%20-%202/movie-bag). The code is stored in the path [API-Exercise/API/titanic_api](API/titanic_api/app).

Important files and folders:
1. /app: app folder
1. /app/app.py: Run file for the app
1. /app/main.py: module to be loaded by uwsgi
1. /app/uwsgi.ini: app-specific uwsgi config
1. /etc/uwsgi/uwsgi.ini: uwsgi general config
1. /app/.venv: Virtual env

__Important to note that uwsgi runs as uwsgi user, not root.__

This is a custom built image. The build process is a multistage:
1. Stage 1: The virtualenv is installed and cleaned up (pip cache, pyc files, ...)
1. Stage 2: The /app dir is copied from the builder, as well as config files, exposed ports

Both stages are based in the image python:3.8-alpine3.11. This generates a, from my point of view, performant 100mb image.

It is important to note that the connection with mongo is defined using the environment variable 'MONGO_URI', and should looks like this:
_mongodb://mongo-host-or-svc:27017/apidb_




# 6. Ansible

Ansible is in charge of deploying the platform. It is placed `API-Exercies/ansible`.

To run the playbook, simply execute: `$ ansible-playbook -i environment/[devel|production] deploy-from-scratch.yml`

## 6.1. Good to know
* There are 2 environmets defined (under the folder environments), each one has it owns vars defined under `environments/[ENVIRONMENT]/group_vars/all.yml`. Here is where most of the config can be done.
* Vars are loaded automatically
* In the root of the playbook there is a file called `secrets_vars.yml.EXAMPLE`. Copy it to secrets_vars.yml and replace the values there. The file won't be tracked by Git.
* 


## 6.2. Roles

For each role the tasks are defined in a single file: `roles/[ROLE]/tasks/main.yml`

### 6.2.1. cluster
In charge of creating the cluster in Google Cloud

### 6.2.2. mongo
Will create the mongo backend

### 6.2.3. endpoint
Deploy the fronted tier (API+nginx), update the DNS and populates the DB.


# 7. Accessing the service
The service can be accessed via:
* Development environment: http://devel-titanic.jdani.eu/people
* Production environment: http://devel-titanic.jdani.eu/people

As the GoDaddy API keys are needed to update the DNS registry and these are not included, the task is **commented out** in roles/endpoint/tasks/main.yml:49. If you want to test it and have a GoDaddy domain:
1. Define api key and secret in secrets_vars.yml
1. Define your subdomain and tld in environments/[ENV]/group_vars/all.yml
1. Uncomment the task in roles/endpoint/tasks/main.yml:49.

The service will last online for one week.


# 8. Improvement points / ToDo
* Add ELK for metrics
* HPA configured but not working as GKE is not able to retrieve CPU and RAM metrics
* Use google.cloud ansible extension to attach snapshot policy to mongo persistent disk. (Could not find how to do that within Ansible...)
* Enable https
* Helm MongoDB ReplicaSet from Bitnami chart is not working with authentication, even copying and pasting the step-by-step guide that the helm install command shows when executed successfully. So it was considered, as this is a test and I do not have that much time, to go on like this. I know it is not a best practice to leave services open without authentication but, in this case, the service just can be reached from within the pods network, so it is good enough to continue by now.
* Implement tests.

