#!/bin/bash

set -x

csvfile='/tmp/titanic.csv'


# Download file into csvfile
openssl s_client -quiet -connect gitlab.com:443 > $csvfile << eof
GET /ContainerSolutions/API-Exercise/-/raw/master/titanic.csv HTTP/1.0
Host: gitlab.com

eof


# Clean file
sed -i -n '/^Survived/,$p' $csvfile

# Rename fields to acoomodate to API Definition
# Replace 1/0 with true/false.
sed -i \
    -e 's/^0/false/' \
    -e 's/^1/true/' \
    -e '1 s/Survived/survived/' \
    -e '1 s/Pclass/passengerClass/' \
    -e '1 s/Name/name/' \
    -e '1 s/Sex/sex/' \
    -e '1 s/Age/age/' \
    -e '1 s|Siblings/Spouses Aboard|siblingsOrSpousesAboard|' \
    -e '1 s|Parents/Children Aboard|parentsOrChildrenAboard|' \
    -e '1 s/Fare/fare/' \
    $csvfile


mongoimport \
    --type csv \
    --headerline \
    --username=$MONGO_INITDB_ROOT_PASSWORD \
    --password=$MONGO_INITDB_ROOT_PASSWORD \
    --db=$MONGO_INITDB_DATABASE
    --collection=Person \
    --host=localhost \
    $csvfile


