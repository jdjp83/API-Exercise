from .db import db
from mongoengine.errors import ValidationError

class Person(db.Document):
    meta = {"collection": "Person"}
    # uuid = db.StringField(primary_key=True)

    survived = db.BooleanField(
        #db_field="Survived",
        required=True
    )
    
    passengerClass = db.IntField(
        min_value=1,
        max_value=3,
        required=True
    )
    
    name = db.StringField(
        required=True,
        #unique=True
    )

    sex = db.StringField(
        required=True,
        choices=['other','female','male']
    )

    age = db.IntField(
        min_value=0,
        max_value=150,
        required=True
    )
    
    siblingsOrSpousesAboard = db.IntField(
        required=True,
        min_value=0
    )

    parentsOrChildrenAboard = db.IntField(
        required=True,
        min_value=0
    )

    fare = db.FloatField(
        required=True
    )
