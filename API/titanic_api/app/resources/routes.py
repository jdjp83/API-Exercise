from .people import PeopleApi, PersonApi

def initialize_routes(api):
    api.add_resource(PeopleApi, '/people')
    api.add_resource(PersonApi, '/people/<id>')
