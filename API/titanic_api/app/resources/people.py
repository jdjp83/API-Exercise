import json
from flask import Response, request
from database.models import Person
from flask_restful import Resource
from flask_mongoengine import DoesNotExist, ValidationError


def id_to_uuid(person):
    # It is needed to return an uuid as a unique identifier for each
    # document/person/row. In order to not create and/or manage it, it
    # seems like a good idea to use the MongoDB Document internal ID.
    # The problem with that is that it is create as a nested json blob
    # in the field _id. The field in that blob containing the id is '$oid'.
    #
    # So the idea here is to add a new field to the json response called uuid
    # containing the id.oid value and remove the id value. This has to be done
    # each method returning a person or people object.
    #
    # If you my dear and smart reader are wondering what happens
    # with the requests for a particular uuid, by any meaning it is well managed.
    # So no further code is needed.
    person['uuid'] = person['_id']['$oid']
    person.pop('_id')
    return person

class PeopleApi(Resource):
    def get(self):
        # Relying on json to cast mongoengine.Document.Object to dict
        # Potentially improve point
        people = json.loads(Person.objects().to_json())
     
        # return mongo id as uuid
        for person in people:
            person = id_to_uuid(person)

        
        return Response(json.dumps(people), mimetype="application/json", status=200)



    def post(self):
        print("* " * 8, "PeopleAPI.post")
        body = request.get_json()
        try:
            person =  Person(**body).save()
        except ValidationError as err:
            return 'Malformed request: {}'.format(err), 401
        id = person.id
        person = json.loads(Person.objects.get(id=str(id)).to_json())
        person = id_to_uuid(person)
        return Response(json.dumps(person), mimetype="application/json", status=200)
        
class PersonApi(Resource):
    def put(self, id):
        print("* " * 8, "PersonAPI.put")
        body = request.get_json()
        try:
            Person.objects.get(id=id).update(**body)
        except DoesNotExist:
            return 'Not found', 404
        except ValidationError as err:
            return 'Malformed request: {}'.format(err), 401
        person = json.loads(Person.objects.get(id=id).to_json())
        person = id_to_uuid(person)
        return Response(json.dumps(person), mimetype="application/json", status=200)
    
    def delete(self, id):
        print("* " * 8, "PersonAPI.delete")
        try:
            Person.objects.get(id=id).delete()
        except DoesNotExist:
            return 'Not found', 404
        except ValidationError as err:
            return 'Malformed request: {}'.format(err), 401
        return '', 200

    def get(self, id):
        print("* " * 8, "PersonAPI.get")
        try:
            person = json.loads(Person.objects.get(id=id).to_json())
        except DoesNotExist:
            return 'Not found', 404
        except ValidationError as err:
            return 'Malformed request: {}'.format(err), 401
        person = id_to_uuid(person)
        return Response(json.dumps(person), mimetype="application/json", status=200)
