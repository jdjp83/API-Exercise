import os
from flask import Flask
from database.db import initialize_db
from flask_restful import Api
from resources.routes import initialize_routes

app = Flask(__name__)
api = Api(app)

MONGO_URI = os.environ['MONGO_URI']

app.config['MONGODB_SETTINGS'] = {
    'host': MONGO_URI,
    # 'host': 'mongodb://mongo/titanic?retryWrites=true&w=majority',
    'connect': False,
    'connectTimeoutMS': 30000,
    'socketTimeoutMS': None,
    'socketKeepAlive': True,
    'maxPoolsize': 1
}

initialize_db(app)
initialize_routes(api)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5000')
